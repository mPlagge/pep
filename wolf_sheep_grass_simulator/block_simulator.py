#!/bin/python

import sys
import time
import pygame
import random
from random import randint

class Block():
    type='clear'
    color=(0,0,0)

    def can_reproduce(self):
        print("THIS MESSAGE IS A BUG!")
        # TODO somehow the parant class gets called 
        # when trying to call the this function for the wolfblock
        return False

    def gain_energy(self):
        print("THIS MESSAGE IS A BUG!")
        # TODO somehow the parant class gets called 
        # when trying to call the this function for the wolfblock

class WolfBlock(Block):
    age=0
    gen=1
    energy=1000
    type='wolf'
    color=(0,0,255)

    def check_die(self):
        if self.age > randint(50000,100000) or self.energy <= 0:
            print("Wolf dies")
            return True
        return False 

    def gain_energy(self):
        self.energy += 10

    def lose_energy(self):
        if randint(0,100) > 90:
            self.energy = self.energy - 1
        self.age += 1

    def can_reproduce(self):
        if self.energy > 2000:
            return True
        return False

    def reproduce(self):
        self.energy -= 1000
        print("New wolf")

    def new_gen(self):
        self.color == (0,255,255)
        self.gen += 1

class SheepBlock(Block):
    age=0
    gen=1
    energy=1000
    type='sheep'
    color=(255,0,0)

    def check_die(self):
        if self.age > randint(90000,200000) or self.energy <= 0:
            print("Sheep dies of starvation")
            return True
        return False 

    def lose_energy(self):
        if randint(0,100) > 50:
            self.energy = self.energy - 1
        self.age += 1

    def gain_energy(self):
        self.energy += 200
        print("SHEEP ENERGY: ",self.energy)
    
    def can_reproduce(self):
        if self.energy > 2000:
            print("New sheep")
            return True
        return False

    def reproduce(self):
        self.energy -= 1000

    def new_gen(self):
        self.color == (255,0,255)
        self.gen += 1

class GrassBlock(Block):
    age=0
    type='grass'
    color=(0,255,0)

    def check_die(self):
        if self.age == randint(50,200):
            return True
        return False

    def age_up(self):
        self.age += 1
        #print(self.age)

class Board():
    grass_counter=1
    sheep_counter=0
    wolf_counter=0
    display=(())
    grid=[]
    display_block_size=5
    display_width=100
    display_height=100
    background_color=(0,0,0)

    def update_screen(self):
        self.display.fill(self.background_color)
        grid = self.grid
        for i in range(self.display_width):
            for j in range(len(grid[1])):
                pygame.draw.rect(
                    self.display,grid[i][j].color,(
                        i*self.display_block_size,
                        j*self.display_block_size,
                        self.display_block_size,
                        self.display_block_size,
                    )
                )
        self.update_score_board()
        pygame.display.update()

    def pygame_setup(self):
        pygame.init()
        pygame.font.init()
        self.display = pygame.display.set_mode((
            self.display_block_size*self.display_width+200,
            self.display_block_size*self.display_height,
        ))
        self.display.fill(self.background_color)

    def make_grid(self):
        self.grid = [[ Block() for y in range(self.display_width)] 
            for x in range(self.display_height)]

        for i in range(50):
            self.grid[randint(0,self.display_width-1)][
                      randint(0,self.display_height-1)] = SheepBlock()

        for i in range(10):
            self.grid[randint(0,self.display_width-1)][
                      randint(0,self.display_height-1)] = WolfBlock()

        for i in range(10):
            self.grid[randint(0,self.display_width-1)][
                      randint(0,self.display_height-1)] = GrassBlock()


    def update_grass(self,i,j):
#        grass_spawn_chance = 50 / len(self.grid[i]) / len(self.grid[i])
#        if randint(0,100) > 100-grass_spawn_chance:
#            spawns=0
#            while spawns < 1:
#                new_pos=[randint(0,self.display_width-1),randint(0,self.display_height-1)]
#
#                if self.grid[new_pos[0]][new_pos[1]].type == Block.type:
#                    self.grid[new_pos[0]][new_pos[1]] = GrassBlock()
#                    spawns+=1

        new_pos=[randint(0,self.display_width-1),randint(0,self.display_height-1)]
        
        if self.grid[new_pos[0]][new_pos[1]].type == Block.type:
            self.grid[new_pos[0]][new_pos[1]] = GrassBlock()
#            spawns+=1

        self.grid[i][j].age_up()
        if self.grid[i][j].check_die():
            self.grid[i][j] = Block()

    def update_sheep(self, i, j):

        while True:
            error=False
            rand_move = [randint(-1,1), randint(-1,1)]

            if i + rand_move[0] > 98:
                error=True
            if j + rand_move[1] > 98:
                error=True
            if i + rand_move[0] < 0:
                error=True
            if j + rand_move[1] < 0:
                error=True

            if error == False:
                break

        self.grid[i][j].lose_energy()

        # kill Sheep 
        if self.grid[i][j].check_die():
            self.grid[i][j] = Block()

        else:
            # go to a block
            if self.grid[i+rand_move[0]][j+rand_move[1]].type == Block.type:
                if self.grid[i][j].can_reproduce() and rand_move != [0,0]:
                    self.grid[i][j].reproduce()
                    self.grid[i+rand_move[0]][j+rand_move[1]] = self.grid[i][j]
                    self.grid[i][j].new_gen()
                    self.grid[i][j].age = 0
                else:
                    self.grid[i+rand_move[0]][j+rand_move[1]] = self.grid[i][j]
                    self.grid[i][j] = Block()


            # go to grass, get energy and replace old block
            if self.grid[i+rand_move[0]][j+rand_move[1]].type == GrassBlock.type:
                self.grid[i][j].gain_energy()
                self.grid[i+rand_move[0]][j+rand_move[1]] = self.grid[i][j]
                self.grid[i][j] = Block()

    def update_wolf(self,i,j):
        self.grid[i][j].lose_energy()
        
        # check if should die
        if self.grid[i][j].check_die():
            self.grid[i][j] = Block()
        
        while True:
            error=False
            rand_move = [randint(-1,1), randint(-1,1)]
        
            if i + rand_move[0] > 98:
                error=True
            if j + rand_move[1] > 98:
                error=True
            if i + rand_move[0] < 0:
                error=True
            if j + rand_move[1] < 0:
                error=True
        
            if error == False:
                break
        
        if self.grid[i+rand_move[0]][j+rand_move[1]].type == Block.type:
            self.grid[i+rand_move[0]][j+rand_move[1]] = self.grid[i][j]
            if self.grid[i][j].can_reproduce():
                self.grid[i][j].reproduce()
                self.grid[i][j].new_gen()
                self.grid[i][j].age = 0
            else:
                self.grid[i][j] = Block()
        
        if self.grid[i+rand_move[0]][j+rand_move[1]].type == SheepBlock.type:
            self.grid[i+rand_move[0]][j+rand_move[1]] = self.grid[i][j]
            self.grid[i][j].gain_energy()
            self.grid[i][j] = Block()
        
    def update_blocks(self):
        
        wolf_counter=0
        sheep_counter=0
        grass_counter=0


        for i in range(len(self.grid[0])-1):
            for j in range(len(self.grid[1])-1):

                if self.grid[i][j].type == WolfBlock.type:
                    wolf_counter += 1
                    self.update_wolf(i,j)
                if self.grid[i][j].type == SheepBlock.type:
                    sheep_counter += 1
                    self.update_sheep(i,j)
                if self.grid[i][j].type == GrassBlock.type:
                    grass_counter += 1
                    self.update_grass(i,j)


        self.sheep_counter = sheep_counter
        self.grass_counter = grass_counter
        self.wolf_counter  = wolf_counter

    def update_score_board(self):
        my_font = pygame.font.Font('/usr/share/fonts/noto/NotoSansMono-Regular.ttf', 25)

        textsurface = my_font.render("Sheep: " + str(self.sheep_counter), False, (255,0,0))
        self.display.blit(textsurface,(self.display_block_size*self.display_width+20,0))

        textsurface = my_font.render("Wolf:  " + str(self.wolf_counter), False, (0,0,255))
        self.display.blit(textsurface,(self.display_block_size*self.display_width+20,30))

        textsurface = my_font.render("Grass: " + str(self.grass_counter), False, (0,255,0))
        self.display.blit(textsurface,(self.display_block_size*self.display_width+20,30*2))


def main():
    board = Board()
    board.pygame_setup()
    board.make_grid()

    while True:
        board.update_blocks()
        board.update_screen()
main()
