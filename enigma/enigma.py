#!/usr/bin/env python

from string import ascii_lowercase

r1 = [12, 24, 1, 25, 3, 2, 22, 19, 8, 7, 21, 14, 11, 23, 9, 6, 10, 20, 5, 13, 17, 0, 15, 16, 18, 4]
#r1 = [4, 10, 12, 5, 11, 6, 3, 16, 21, 25, 13, 19, 14, 22, 24, 7, 23, 20, 18, 15, 0, 8, 1, 17, 2, 9]
r2 = [0, 9, 3, 10, 18, 8, 17, 20, 23, 1, 11, 7, 22, 19, 12, 2, 16, 6, 25, 13, 15, 24, 5, 21, 14, 4]
r3 = [1, 3, 5, 7, 9, 11, 2, 15, 17, 19, 23, 21, 25, 13, 24, 4, 8, 22, 6, 0, 10, 12, 20, 18, 16, 14]

def rotate_forward(ascii_decimal):

    shift = r1[ascii_decimal-97]
    if shift + ascii_decimal > 122: shift -= 26
    print('C', ascii_decimal, chr(ascii_decimal))
    return ascii_decimal + shift

def rotate_backward(ascii_decimal):

    print('O', ascii_decimal, chr(ascii_decimal))
    for i, shift in enumerate(r1):
        print('fdsff')
        if i + shift == ascii_decimal - 97:
            return ascii_decimal - shift
        elif i + shift - 26 == ascii_decimal - 97:
            print('D',i,  ascii_decimal - shift + 26)
            return ascii_decimal - shift + 26 + i
        else:
            print('W', i, shift, ascii_decimal)

def encrypt(ascii_decimals):

    for i, dec in enumerate(ascii_decimals):
        ascii_decimals[i] = rotate_forward(ascii_decimals[i])
        ascii_decimals[i] = rotate_backward(ascii_decimals[i])

    return ascii_decimals

def main():
    while True:
        msg = input('Enigma >>> ')
        msg = msg.lower()
        if msg == '' or False in [x in ascii_lowercase + ' ' for x in msg]: break
        msg_numbers = [(ord(i)) for i in msg if i != ' ']
        print(''.join([chr(number) for number in encrypt(msg_numbers)]))

if __name__ == '__main__':
    main()
