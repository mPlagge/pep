-- script to get head or tails

import System.Random

headsOrTails :: IO String
headsOrTails = do
    index <- randomRIO (0, 1) :: IO Int
    let choices = ["Heads","Tails"]
    let result = choices !! index
    return result

main = do
    headsOrTails >>= \l -> putStrLn l
