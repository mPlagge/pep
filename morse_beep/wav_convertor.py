#!/usr/bin/env python

import wave

def input_to_formatted_binary_signal(morse_part_lenght=0.05):

    wave_file = wave.open('./out.wav', 'rb')
    
    nframes = wave_file.getnframes()
    frames = wave_file.readframes(nframes)
    
    rate = wave_file.getframerate()
    duration = nframes / float(rate)
    morse_parts = int(duration / morse_part_lenght)
    
    #for sample in range(0, len(frames), int(nframes/morse_parts)):
    #    print(frames[sample], frames[sample+1], frames[sample+1])
    
    message_in_hight_low = []
    
    for sample_index in range(0, len(frames), int(len(frames)/morse_parts)):
        sample = frames[sample_index:sample_index+9]
        if sample[2] != 0:
            message_in_hight_low += [1]
        else:
            message_in_hight_low += [0]
    
    return message_in_hight_low

def main():
    print(input_to_formatted_binary_signal())

if __name__ == '__main__':
    main()
