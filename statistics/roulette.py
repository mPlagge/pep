#!/bin/python
# simulate russian roulette
# >Russian roulette.
# >6 People in a circle take turns
# >6 Chambers
# >1 Bullet
# >1 Game
# >All take turns pulling trigger until one dies.
# >If 5th person fires a blank, he then shoots the 6th.
# 
# The question is:
# Before the game starts, you have a choice of going 1st, 2nd, 3rd, 4th, 5th, or 6th
# Which do you choose and why?

import random

def play(people_losses):
    bullets=[0] * 6
    bullets[random.randint(0,len(bullets)-1)] = 1

    for i  in range(len(people_losses)):
        if bullets[i] == 1:
            people_losses[i] += 1
            return people_losses

def main():
    people_losses=[0] * 6
    for i in range(100000):
        people_losses = play(people_losses)
    print(people_losses)

main()
